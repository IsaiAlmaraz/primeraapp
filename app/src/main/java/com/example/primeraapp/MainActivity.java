package com.example.primeraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Toast.makeText(this, "Hola Mundo", Toast.LENGTH_SHORT).show();

        // botón creado
        Button btn = (Button) findViewById(R.id.btn_nuevo);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(MainActivity.this, "Hola desde mi elemento", Toast.LENGTH_SHORT).show();
                EditText nombre = (EditText) findViewById(R.id.editNombre);
                Intent i = new Intent(v.getContext(),SegundoActivity2.class);
                i.putExtra("nombre", nombre.getText().toString());
                i.putExtra("color", "rojo");
                startActivity(i);
//                Toast.makeText(MainActivity.this, nombre.getText().toString(), Toast.LENGTH_LONG ).show();
            }
        });
    }

//    @Override
//    protected void onStart(){
//        super.onStart();
//        Toast.makeText(this, "On Start", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    protected void onResume(){
//        super.onResume();
//        Toast.makeText(this, "On Resume", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    protected void onPause(){
//        super.onPause();
//        Toast.makeText(this, "On Pause", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    protected void onStop(){
//        super.onStop();
//        Toast.makeText(this, "On Stop", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    protected void onRestart(){
//        super.onRestart();
//        Toast.makeText(this, "On Restar", Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    protected void onDestroy(){
//        super.onDestroy();
//        Toast.makeText(this, "On Destroy", Toast.LENGTH_SHORT).show();
//    }


}