package com.example.primeraapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class SegundoActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundo2);

        String color  = getIntent().getExtras().getString("color");
        String nombre  = getIntent().getExtras().getString("nombre");
        TextView textViewNombre = (TextView) findViewById(R.id.textViewNombre);
        textViewNombre.setText("Bienvenidos " + nombre);

//        Toast.makeText(this, color, Toast.LENGTH_LONG).show();
    }
}